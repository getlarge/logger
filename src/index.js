/* eslint-disable no-console */

/**
 * @method logger
 * @param {int} priority - Logger mode.
 * @param {string} collectionName - service name.
 * @param {string} command - service command to log.
 * @param {string} content - log content.
 */
function logger(priority, collectionName, command, content) {
  // define priority based on process.env.NODE_ENV
  const logLevel = Number(process.env.SERVER_LOGGER_LEVEL) || 4;
  let fullContent;
  const maxLineSize = 250;
  if (
    process.env.NODE_ENV === 'test' ||
    process.env.NODE_ENV === 'development'
  ) {
    if (priority <= logLevel) {
      if (typeof content === 'object') {
        if (content instanceof Error) {
          fullContent = `[${collectionName.toUpperCase()}] ${command} : ${
            content.message
          }`;
        } else {
          fullContent = `[${collectionName.toUpperCase()}] ${command} : ${JSON.stringify(
            content,
          )}`;
        }
      } else if (typeof content !== 'object') {
        fullContent = `[${collectionName.toUpperCase()}] ${command} : ${content}`;
      }
      if (typeof fullContent === 'string' && fullContent.length > maxLineSize) {
        fullContent = `${fullContent.substring(0, maxLineSize - 3)} ...`;
      }
      console.log(fullContent);
      return fullContent;
    } else if (priority > logLevel) {
      return null;
    }
    throw new Error('INVALID_LOG, Missing argument in logger');
  }
  return null;
}

module.exports = logger;
