const {assert} = require('chai');
const logger = require('../index');

describe('logger - test 1', () => {
	const content1 = {topic: 'Aloes123-out/0/3349/3/5910', payload: 'test'};
	const content2 = '1,2,1,2';
	const content3 = new Error('1,2,1,2');

	it(`should not log if process.env.SERVER_LOGGER_LEVEL < logger 1st argument`, () => {
		process.env.SERVER_LOGGER_LEVEL = 3;
		const res = logger(4, 'LOGGER', 'test1', content1);
		assert.strictEqual(null, res);
	});

	it(`should log if process.env.SERVER_LOGGER_LEVEL >= logger 1st argument`, () => {
		process.env.SERVER_LOGGER_LEVEL = 4;
		const res = logger(4, 'LOGGER', 'test2', content2);
		assert.strictEqual(`[LOGGER] test2 : ${content2}`, res);
	});

	it(`should log stringified object`, () => {
		process.env.SERVER_LOGGER_LEVEL = 4;
		const res = logger(4, 'LOGGER', 'test3', content1);
		assert.strictEqual(`[LOGGER] test3 : ${JSON.stringify(content1)}`, res);
	});

	it(`should log error object message`, () => {
		process.env.SERVER_LOGGER_LEVEL = 4;
		const res = logger(4, 'LOGGER', 'test4', content3);
		assert.strictEqual(`[LOGGER] test4 : ${content3.message}`, res);
	});
});
