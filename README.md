# Aloes Logger

- Console.log wrapper

---

## Installation

With npm :

```bash
$ npm install aloes-logger --save
```

## Linting

With ESLint

```bash
$ npm run lint
```

## Test

With Mocha

```bash
$ npm run test
```
